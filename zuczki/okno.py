from grafika import Gra, plik_graf_gry, plik_graf_cyklu
from logika import generuj_zagadke
import os
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk


class Okno_Gry(Gtk.VBox):
    def __init__(self, per, lista_cykli):
        super().__init__()
        self.aktualna_gra = Gra(per, lista_cykli)
        self.set_border_width(10)
        self.pudlo_planszy = Gtk.Box()
        self.wyswietl_aktualna_plansze()
        self.pasek_cykli = Gtk.HBox()
        for graf_cyklu in self.aktualna_gra.ruchy:
            self.dorzuc_cykl_na_pasek(graf_cyklu)
        self.pack_start(self.pudlo_planszy, True, True, 20)
        self.pack_start(self.pasek_cykli, True, True, 20)
        self.show_all()

    def wyswietl_aktualna_plansze(self):
        im = Gtk.Image()
        im.set_from_file(plik_graf_gry)
        dzieci = self.pudlo_planszy.get_children()
        if len(dzieci) > 0:
            self.pudlo_planszy.remove(dzieci[0])
        self.pudlo_planszy.pack_start(im, True, True, 30)
        self.show_all()

    def dorzuc_cykl_na_pasek(self, graf_cyklu):
        obraz_cyklu = Gtk.Image()
        obraz_cyklu.set_from_file(plik_graf_cyklu(graf_cyklu.nr))
        guzik_cyklu = Gtk.Button()
        guzik_cyklu.set_image(obraz_cyklu)
        guzik_cyklu.connect("clicked", self.wykonaj_ruch, graf_cyklu.nr)
        self.pasek_cykli.pack_start(guzik_cyklu, True, False, 10)

    def wykonaj_ruch(self, guzik, *arg):
        nr_cyklu = arg[0]
        uruchomiony_cykl = self.aktualna_gra.lista_cykli[nr_cyklu]
        self.aktualna_gra.dzialaj_cyklem(uruchomiony_cykl)
        self.wyswietl_aktualna_plansze()
        if self.aktualna_gra.czy_wygrana():
            pudlo_glowne.wyswietl_okno_zwyciestwa()

    def resetuj_gre(self):
        self.aktualna_gra.resetuj()
        self.wyswietl_aktualna_plansze()

    def posprzataj_gre(self):
        ile_cykli = len(self.aktualna_gra.lista_cykli)
        if os.path.isfile(plik_graf_gry):
            os.remove(plik_graf_gry)
        for nr in range(ile_cykli):
            if os.path.isfile(plik_graf_cyklu(nr)):
                os.remove(plik_graf_cyklu(nr))


class Okno_Aplikacji(Gtk.VBox):
    def __init__(self):
        super().__init__()
        self.uloz_pasek_menu()
        self.okno_gry = Gtk.VBox()
        self.ustaw_poziom(1)
        self.podlacz_przyciski(self.reset, self.nowa, self.zmien)
        self.uruchom_gre()
        self.pack_start(self.pasek_menu, True, True, 10)

    def uloz_pasek_menu(self):
        self.pasek_menu = Gtk.HBox()
        self.reset = Gtk.Button("RESETUJ GRĘ")
        self.nowa = Gtk.Button("NOWA GRA")
        self.zmien = Gtk.Button("ZMIEŃ POZIOM")
        self.poziom = Gtk.Label("tu będzie nazwa poziomu")
        self.pasek_menu.pack_start(self.reset, True, True, 10)
        self.pasek_menu.pack_start(self.nowa, True, True, 10)
        self.pasek_menu.pack_start(self.zmien, True, True, 10)
        self.pasek_menu.pack_start(self.poziom, True, True, 10)
        self.okno_zwyciestwa = None
        self.okno_zmian = None

    def podlacz_przyciski(self, reset, nowa, zmien):
        reset.connect("clicked", self.wykonaj_reset)
        nowa.connect("clicked", self.wykonaj_nowa_gra)
        zmien.connect("clicked", self.wyswietl_okno_zmiany)

    def ustaw_poziom(self, i):
        self.obecny_poziom = i
        self.poziom.set_text("POZIOM: " + str(i))

    def uruchom_gre(self):
        permutacja_gry, cykle = generuj_zagadke(self.obecny_poziom)
        self.remove(self.okno_gry)
        self.okno_gry = Okno_Gry(permutacja_gry, cykle)
        self.pack_end(self.okno_gry, True, True, 10)

    def wykonaj_reset(self, guzik):
        self.okno_gry.resetuj_gre()
        if self.okno_zwyciestwa is not None:
            self.okno_zwyciestwa.destroy()

    def wykonaj_nowa_gra(self, guzik):
        self.okno_gry.posprzataj_gre()
        self.uruchom_gre()
        if self.okno_zwyciestwa is not None:
            self.okno_zwyciestwa.destroy()

    def wykonaj_ustaw_poziom(self, guzik, i):
        self.ustaw_poziom(i)
        self.uruchom_gre()
        self.okno_zmiany.destroy()

    def wyswietl_okno_zmiany(self, guzik):
        if self.okno_zwyciestwa is not None:
            self.okno_zwyciestwa.destroy()
        self.okno_zmiany = Gtk.Dialog(window, title="zmiana poziomu")
        self.okno_zmiany.set_default_size(100, 200)
        zawartosc = self.okno_zmiany.get_content_area()
        etykieta = Gtk.Label("Wybierz nowy poziom:")
        zawartosc.pack_start(etykieta, True, True, 20)
        pasek_guzikow = Gtk.HBox()
        for i in range(1, 7):
            guzik_poziomu = Gtk.Button("     " + str(i) + "     ")
            pasek_guzikow.pack_start(guzik_poziomu, True, True, 15)
            guzik_poziomu.connect("clicked", self.wykonaj_ustaw_poziom, i)
        zawartosc.pack_start(pasek_guzikow, True, True, 20)
        self.okno_zmiany.set_transient_for(window)
        self.okno_zmiany.show_all()
        self.umiesc_na_srodku(self.okno_zmiany)
        self.okno_zmiany.run()

    def wyswietl_okno_zwyciestwa(self):
        self.okno_zwyciestwa = Gtk.Dialog(window, title="Zwycięstwo!")
        self.okno_zwyciestwa.set_default_size(100, 200)
        zawartosc = self.okno_zwyciestwa.get_content_area()
        etykieta = Gtk.Label("Gratulacje, wygrałeś!")
        zawartosc.pack_start(etykieta, True, True, 20)
        reset = Gtk.Button("JESZCZE RAZ")
        nowa = Gtk.Button("NOWA GRA")
        zmien = Gtk.Button("ZMIEŃ POZIOM")
        pasek_guzikow = Gtk.HBox()
        pasek_guzikow.pack_start(reset, True, True, 10)
        pasek_guzikow.pack_start(nowa, True, True, 10)
        pasek_guzikow.pack_start(zmien, True, True, 10)
        self.podlacz_przyciski(reset, nowa, zmien)
        zawartosc.pack_start(pasek_guzikow, True, True, 20)
        self.okno_zwyciestwa.set_transient_for(window)
        self.okno_zwyciestwa.show_all()
        self.umiesc_na_srodku(self.okno_zwyciestwa)
        self.okno_zwyciestwa.run()

    def umiesc_na_srodku(self, okno_dialogu):
        x = window.get_position()[0]
        y = window.get_position()[1]
        dx = window.get_size()[0]/2
        dy = window.get_size()[1]/2
        wlasny_dx = okno_dialogu.get_size()[0]/2
        wlasny_dy = okno_dialogu.get_size()[1]/2
        okno_dialogu.move(x + dx - wlasny_dx, y + dy - wlasny_dy)


window = Gtk.Window(title="PERMUTUJĄCE ŻUCZKI")
window.show()
window.connect("destroy", Gtk.main_quit)
pudlo_glowne = Okno_Aplikacji()
window.add(pudlo_glowne)
window.show_all()
Gtk.main()
